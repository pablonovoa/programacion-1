#include <stdio.h>

int main(){
    unsigned int vocalCount = 0;
    char letra;

    do{
        printf("Ingrese una letra (para terminar ingrese *):  ");
        fflush(stdin);
        scanf("%c", &letra);
        //getchar(); // descomentar esto para linux
        switch(letra){
            case 'A':
            case 'E':
            case 'I':
            case 'O':
            case 'U':
            case 'a':
            case 'e':
            case 'i':
            case 'o':
            case 'u':
                vocalCount++;
                break;
        }
    }while(letra != '*');
    
    printf("\nCantidad de vocales ingresadas: %i", vocalCount);

    printf("\n");

    return 0;
}