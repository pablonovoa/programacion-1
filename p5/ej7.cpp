#include <stdio.h>

int main(){
    
    float capacidad;
    printf("Ingrese una capacidad: ");
    scanf("%f", &capacidad);
    
    // Versión 1
    if(capacidad > 1 || capacidad < 0)
        printf("La capacidad ingresada no es válida");
    else if(capacidad <= 0.3)
        printf("Vacía");
    else if(capacidad <= 0.7)
        printf("Media");
    else
        printf("Llena");


    // no se puede hacer la versión 2 con switch porque estoy chequeando un float
    

    printf("\n");

    return 0;
}