#include <stdio.h>

void patern1(int a, int b){
    printf("Patron 012...a de b lineas\n\n");
    for(int i=1; i <= b; i++){
        for(int j=0; j<=a; j++) 
            printf("%i", j);
        printf("\n");
    }
}

void patern2(int a, int b){
    printf("Patron xxx (con a cantidad de x, con x de 0 a b)\n\n");
    for(int i=0; i <= b; i++){
        for(int j=1; j<=a; j++) 
            printf("%i", i);
        printf("\n");
    }
}

void patern3(int a, int b){
    printf("Patron a(a-1)...210  de b lineas\n\n");
    for(int i=1; i <= b; i++){
        for(int j=a; j>=0; j--) 
            printf("%i", j);
        printf("\n");
    }
}

void patern4(int a, int b){
    printf("Patron xxx (con a cantidad de x, con x de b a 0)\n\n");
    for(int i=b; i >= 0; i--){
        for(int j=1; j<=a; j++) 
            printf("%i", i);
        printf("\n");
    }
}

int main(){
    int a, b, pattern;
    printf("\nIngrese dos numeros: (a y b) \n");
    scanf("%i%i", &a, &b);

    printf("\nIngrese el patron a imprimir: (1-2-3-4) \n");
    scanf("%i", &pattern);

    printf("\n-----------------------------------------\n");
    switch (pattern){
        case 1:
            patern1(a, b);
            break;
        case 2:
            patern2(a, b);
            break;
        case 3:
            patern3(a, b);
            break;
        case 4:
            patern4(a, b);
            break;
        default:
            printf("Error: patron invalido");
            break;
    }
    printf("\n-----------------------------------------\n");

    return 0;
}