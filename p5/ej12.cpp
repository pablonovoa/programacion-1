#include <stdio.h>

int main(){
    int a, b, resultado=1;
    printf("Ingrese dos numeros: \n");
    scanf("%i%i", &a, &b);

    if(b<0){
        printf("\nError: b no puede ser negativo");
    }else{
        for(int i=1; i<=b; i++){
            resultado = resultado * a;
        }
    }

    printf("Resultado de %i^%i: %i\n", a, b, resultado);

    return 0;
}