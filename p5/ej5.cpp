#include <stdio.h>

int main(){
    int a;
    int b;

    printf("Ingrese un número: ");
    scanf("%i", &a);
    printf("Ingrese otro número: ");
    scanf("%i", &b);

    if(b==0){
        printf("ERROR: b tiene que ser distinto de cero\n");
        return 0;
    }
    
    printf("Cociente: %i\n", a/b);
    printf("Resto: %i\n", a%b);
    

    return 0;
}