#include <stdio.h>

int main(){
    int d, m, a;
    printf("Ingrese una fecha: \n");
    scanf("%i%i%i", &d, &m, &a);

    switch(m){
        case 1:
        case 3:
        case 5:
        case 7:
        case 8:
        case 10:
        case 12:
            if(d > 31 || d < 1) printf("La fecha está mal, tiene mal el dia");
            else printf("La fecha bien");
            break;
        case 4:
        case 6:
        case 9:
        case 11:
            if(d > 30 || d < 1) printf("La fecha está mal, tiene mal el dia");
            else printf("La fecha bien");
            break;
        case 2:
            if( 
                d<1 ||
                (a%4==0 && d>29) ||
                (a%4!=0 && d>28)
            ) printf("La fecha está mal, tiene mal el dia");
            else printf("La fecha bien");
            break;
        default:
            printf("La fecha esta mal, el mes no esiste");
            break;
    }
    printf("\n");

    return 0;
}