#include <stdio.h>

int main(){
    char letra;
    printf("Ingrese una letra: ");
    scanf("%c", &letra);

    if(letra >= 'A' && letra <= 'Z'){
        printf("\nLa letra es mayuscula\n");
    }else if(letra >= 'a' && letra <= 'z'){
        printf("\nLa letra es minsucula\n");
    }else if(letra >= '0' && letra <= '9'){
        printf("\nLa letra es un digito\n");
    }else{
        printf("\nLa letra es un signo\n");
    }

    return 0;
}