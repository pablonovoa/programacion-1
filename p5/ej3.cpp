#include <stdio.h>

int main(){
    int num1;
    int num2;

    printf("Ingrese un número: ");
    scanf("%i", &num1);
    printf("Ingrese otro número: ");
    scanf("%i", &num2);

    if(num1 > num2)
        printf("%i\n", num1);
    else if(num1 == num2)
        printf("Son iguales\n");
    else
        printf("%i\n", num2);
    

    return 0;
}