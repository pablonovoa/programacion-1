#include <stdio.h>

int main(){
    int count = 0;
    char continuar;

    for(int i=32; i<=256; i++){
        printf("\n%i - %c", i, i);
        count++;

        if(count == 20){
            count = 0;
            printf("\nToque una enter para continuar");
            fflush(stdin);
            scanf("%c", &continuar);
            //getchar(); // descomentar esto para linux
        }
    }
    

    printf("\n");

    return 0;
}