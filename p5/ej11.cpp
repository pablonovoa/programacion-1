#include <stdio.h>

int main(){
    int num, odd=0, even=0;

    for(int i=1; i<=10; i++){
        printf("\nIngrese un numero: ");
        scanf("%i", &num);
        if(num%2 == 0)
            even += num;
        else 
            odd += num;
    }

    printf("\nTotal suma pares: %i\nTotal suma impares: %i\n", even, odd);

    return 0;
}