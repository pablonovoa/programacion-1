#include <stdio.h>

int main(){
    int a, b, resultado=1;
    printf("Ingrese dos numeros: \n");
    scanf("%i%i", &a, &b);

    printf("\n--------------------------------\n");
    printf("Multiplos de %i entre 0 y %i", b, a);
    for (int i=0; i<=a; i++){
        if(i%b==0)
            printf("\n%i", i);
    }
    printf("\n--------------------------------\n");

    return 0;
}