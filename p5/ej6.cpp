#include <stdio.h>

int main(){
    
    unsigned int nota;
    printf("Ingrese una nota: ");
    scanf("%i", &nota);
    
    // Version 1
    if(nota > 12 || nota < 0)
        printf("La nota ingresada no es válida");
    else if(nota <= 3)
        printf("Reprobado");
    else if(nota <= 7)
        printf("Aceptable");
    else if(nota <= 11)
        printf("Bueno");
    else
        printf("Excelente");


    // Version 2: usando otra estructura de control
    /*
    switch (nota){
        case 0:
        case 1:
        case 2:
        case 3:
            printf("Reprobado");
            break;
        case 4:
        case 5:
        case 6:
        case 7:
            printf("Aceptable");
            break;
        case 8:
        case 9:
        case 10:
        case 11:
            printf("Bueno");
            break;
        case 12:
            printf("Excelente");
            break;
        default:
            printf("La nota ingresada no es válida");
            break;
    }
    */
    

    printf("\n");

    return 0;
}