#include <stdio.h>

int main(){
    char letra;
    printf("Ingrese una letra: ");
    scanf("%c", &letra);

    if(letra >= 'A' && letra <= 'Z'){
        printf("\nLa letra es mayuscula\n");
    }else{
        printf("\nLa letra NO es mayuscula\n");
    }
}