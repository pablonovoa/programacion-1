#include <stdio.h>

int main(){
    float num1;
    float num2;
    float num3;

    printf("Ingrese un número: ");
    scanf("%f", &num1);
    printf("Ingrese otro número: ");
    scanf("%f", &num2);
    printf("Otro mas: ");
    scanf("%f", &num3);

    float mayor;
    float menor;

    if(num1 >= num2 && num1 >= num3)
        mayor = num1;
    else if( num2 >= num3 )
        mayor = num2;
    else 
        mayor = num3;


    if(num1 <= num2 && num1 <= num3)
        menor = num1;
    else if( num2 <= num3 )
        menor = num2;
    else 
        menor = num3;    

    printf("Mayor: %.3f1\n", mayor);
    printf("Menor: %.3f\n", menor);

    return 0;
}