#include <iostream>

using namespace std;

float factorial(float x){
    if (x < 0)
        return -1;
    if (x == 0)
        return 1;
    return x * factorial(x-1);
}

int main(){
    float num;
    cout << "Ingrese un numero para hacerle el factorial: ";
    cin >> num;

    float resultado = factorial(num);
    
    cout << "--------------------------------------" << endl;
    if(resultado > 0)
        cout << "el factorial de "<< num << " es: " << resultado << endl;
    else 
        cout << "Error"<<endl;
    cout << "--------------------------------------" << endl;

    return 0;
}