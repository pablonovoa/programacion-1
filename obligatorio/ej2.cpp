#include <stdio.h>
#include <cstdlib>
#include <ctime>
 /**
 * DesComentar este bloque en linux
 */
/*
*/
#include <stdio_ext.h>
int fflush(FILE * bufferAborrar){ __fpurge(bufferAborrar); return 0; }

const int N = 2;
typedef int laMatrix[N][N];
typedef enum {FALSE, TRUE} boolean;
typedef enum {FILA, COLUMNA, DPRINCIPAL, DSECUNDARIA, NINGUNO} TipoDeJuego;

int main(){

    laMatrix matrix;
    TipoDeJuego tipoJuego = NINGUNO;
    boolean ningunaEsCorrecto, seguirJugando=TRUE;
    int i, j, intentosMax, intentosCount=0, aciertos=0, randNum, posFilaCol=0, suma=0;
    char seguirInput = 's';
    float ratioAciertos;

    printf("\n");
    printf("----------------------------------------------------\n");
    printf("   Bienvenido al super mega juego de LA MATRIX");
    printf("\n----------------------------------------------------");
    printf("\n");
    printf("\nCargue valores para la matrix (enteros entre 0 y 9):\n");
    for(i=0; i<N; i++){
        for(j=0; j<N; j++){
            printf("[%i-%i]: ", i, j);
            scanf("%i", &matrix[i][j]);
            while(matrix[i][j]<0 || matrix[i][j]>9){
                printf("\nValor incorrecto, ingreselo de nuevo: ");
                fflush(stdin); //esto es por si el usuario ingresa una letra.
                scanf("%i", &matrix[i][j]);
            }
        }
    }

    printf("\n-------------| La Matrix |-------------\n");
    for(i=0; i<N; i++){
        for(j=0; j<N; j++){
            printf("%i ", matrix[i][j]);
        }
        printf("\n");
    }
    printf("---------------------------------------\n");

    printf("\nCuantos intentos quiere jugar?: ");
    scanf("%i", &intentosMax);
    if(intentosMax == 0){
        printf("\nBo, no es tan aburrido, la proxima dale una chance, bueno, chau...\n");
    }else{

        printf("\n\n");

        while(intentosCount<intentosMax && seguirJugando){
            printf("\n----------------| Intento %i |--------------------\n", intentosCount+1);
            srand((int) std::time(0));
            randNum = std::rand()%(9*N+1);
            
            printf("El numero alazar para este intento es: %i\n", randNum);

            i=0;
            printf("\nSeleccione tipo de juego\n[1-Fila, 2-Columna, 3-Diagonal Principal, 4-Diagonal Secundaria, 5-Ninguno]: (opción por defecto: 5) \n");
            scanf("%i", &i);
            switch (i){
                case 1:
                    tipoJuego = FILA;
                    break;
                case 2:
                    tipoJuego = COLUMNA;
                    break;
                case 3:
                    tipoJuego = DPRINCIPAL;
                    break;
                case 4:
                    tipoJuego = DSECUNDARIA;
                    break;
                default:
                    tipoJuego = NINGUNO;
                    break;
            }

            if(tipoJuego == FILA || tipoJuego == COLUMNA){
                printf("\nIngrese la posición (entre 0 y %i): ", N-1);
                scanf("%i", &posFilaCol);
            }
            
            suma = 0;
            ningunaEsCorrecto = FALSE;
            switch (tipoJuego){
                case FILA:
                    for(j=0; j<N; j++)
                        suma += matrix[posFilaCol][j];
                    break;
                case COLUMNA:
                    for(i=0; i<N; i++)
                        suma += matrix[i][posFilaCol];
                    break;
                case DPRINCIPAL:
                    for(i=0; i<N; i++)
                        suma += matrix[i][i];
                    break;
                case DSECUNDARIA:
                    for(i=0; i<N; i++)
                        suma += matrix[i][N-i];
                    break;
                default:
                    ningunaEsCorrecto = TRUE;
                    //revisa las filas
                    i=0;
                    while( i<N && ningunaEsCorrecto){
                        suma = 0;
                        for(j=0; j<N; j++){
                            suma += matrix[i][j];
                        }
                        if(suma == randNum)
                            ningunaEsCorrecto = FALSE;
                        i++;
                    }
                    //revisa columnas
                    i=0;
                    while( i<N && ningunaEsCorrecto){
                        suma = 0;
                        for(j=0; j<N; j++){
                            suma += matrix[j][i];
                        }
                        if(suma == randNum)
                            ningunaEsCorrecto = FALSE;
                        i++;
                    }
                    //revisa diag principal
                    suma = 0;
                    for(i=0; i<N; i++){
                        suma += matrix[i][i];
                    }
                    if(suma == randNum)
                        ningunaEsCorrecto = FALSE;
                    //revisa diag sec
                    suma = 0;
                    for(i=0; i<N; i++){
                        suma += matrix[i][N-i];
                    }
                    if(suma == randNum)
                        ningunaEsCorrecto = FALSE;
                    break;
            }

            if(suma == randNum || ningunaEsCorrecto){
                printf("\n***********************\n");
                printf("Embocastes!!! ( ͡° ͜ʖ ͡° ) ");
                printf("\n***********************\n");
                aciertos++;
            }else{
                printf("\n*********************\n");
                printf("No, no embocastes :‑(");
                printf("\n*********************\n");
            }

            intentosCount++;
            if(intentosCount < intentosMax){
                printf("\nQueres seguir jugando (s/n)?\nTe quedan %i intentos :  ", intentosMax-intentosCount);
                seguirInput = 's';
                fflush(stdin);
                scanf("%c", &seguirInput);
                if(seguirInput=='n' || seguirInput=='N')
                    seguirJugando = FALSE;
            }


        }

        ratioAciertos = (aciertos*1.0)/intentosCount;
        printf("\nBien, veamos como le fué...");
        printf("\n\n");
        printf("Total intentos: %i\n", intentosCount);
        printf("Total aciertos: %i\n", aciertos);
        printf("Total fracasos: %i\n", intentosCount-aciertos);


        if(ratioAciertos >= 0 && ratioAciertos <= (1/3.0) )
            printf("\nHay que seguir practicando las sumas!");
        else if(ratioAciertos <= (2/3.0) )
            printf("\nNo está mal, pero puede y debe mejorar.");
        else if(ratioAciertos <=  (intentosCount-1))
            printf("\nMuy buen resultado");
        else 
            printf("\nRendimiento impecable!");


    } // fin del else de cuando pregunta cuantas veces jugar


    printf("\n");
    printf("\n");
    
    return 0;
}