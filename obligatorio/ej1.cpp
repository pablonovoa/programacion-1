#include <stdio.h>
 /**
 * DesComentar este bloque en linux
 */
/*
*/
#include <stdio_ext.h>
int fflush(FILE * bufferAborrar){ __fpurge(bufferAborrar); return 0; }

const int MAX = 300;
typedef char Cadena[MAX];
typedef enum {FALSE, TRUE} boolean;

int main(){
    int i=0, j=0, cantidadMaximaPal, contadorPalabras=0;
    Cadena texto, frase, palabraClave;
    boolean leyendoFrase=FALSE, leyendoPalabra=FALSE, palabraClaveUsada=FALSE, esAlfaNumeric;
    // 1
    printf("Ingrese un texto: ");
    
    fflush(stdin);
    scanf("%c", &texto[i]);
    while (i<MAX-1 && texto[i]!='\n'){
        i++;
        scanf("%c", &texto[i]);
    }
    texto[i] = '\0';
    
    // 2
    i = 0;
    printf("\n");
    while(i<MAX && texto[i] != '\0'){
        esAlfaNumeric = boolean(texto[i]>='A' && texto[i]<='Z' || texto[i]>='a' && texto[i]<='z' || texto[i]>='0' && texto[i]<='9' || texto[i]>=128 && texto[i]<=165);

        if(!leyendoFrase && esAlfaNumeric ){
            leyendoFrase = TRUE;
            j=i;
        }


        if(leyendoFrase){
            if(texto[i] == '.'){
                leyendoFrase = FALSE;
                if(texto[j]>='A' && texto[j]<='Z' || texto[j]>='0' && texto[j]<='9'){
                    printf("\nLa siguiente frase es correcta: ");
                }else{
                    printf("\nLa siguiente frase NO es correcta: ");
                }
                while(j<i && texto[j] !='\0'){
                    printf("%c", texto[j]);
                    j++;
                }
                
            }
        }


        i++;
    }

    if(leyendoFrase){
        printf("\nLa siguiente frase NO es correcta: ");
        while(j<MAX && texto[j] !='\0'){
            printf("%c", texto[j]);
            j++;
        }
    }

    // 3
    printf("\n\nIngrese la cantidad máxima de palabras: ");
    scanf("%i", &cantidadMaximaPal);
   
    i=0;
    while(i<MAX && texto[i] !='\0'){
        esAlfaNumeric = boolean(texto[i]>='A' && texto[i]<='Z' || texto[i]>='a' && texto[i]<='z' || texto[i]>='0' && texto[i]<='9' || texto[i]>=128 && texto[i]<=165);

        if(!leyendoPalabra && esAlfaNumeric ){
            leyendoPalabra = TRUE;
            contadorPalabras++;
        }

        if(leyendoPalabra && !esAlfaNumeric ){
            leyendoPalabra = FALSE;
        }

        i++;
    }
    

    printf("El texto tiene %i palabras", contadorPalabras);
    if(contadorPalabras > cantidadMaximaPal)
        printf(", se exede del maximo por %i palabras", contadorPalabras-cantidadMaximaPal);
    
    // 4
    printf("\nIngrese la palabra clave: ");
    i=0;
    fflush(stdin);
    scanf("%c", &palabraClave[i]);
    while (i<MAX-1 && palabraClave[i]!='\n'){
        i++;
        scanf("%c", &palabraClave[i]);
    }
    palabraClave[i] = '\0';

    j=0;
    while(j<MAX && palabraClave[j] != '\0'  ){
        if(palabraClave[j]>='A' && palabraClave[j]<='Z' )
            palabraClave[j] = palabraClave[j]+32;
        j++;
    }
    i=0;
    while(i<MAX && texto[i] != '\0'  ){
        if(texto[i]>='A' && texto[i]<='Z' )
            texto[i] = texto[i]+32;
        i++;
    }

    i=0; 
    j=0;
    while (i<MAX && texto[i] != '\0' && !palabraClaveUsada){
        if(palabraClave[0] == texto[i]){
            j=0;
            palabraClaveUsada = TRUE;
            while(j<MAX && palabraClave[j] != '\0' && palabraClaveUsada ) {
                if(palabraClave[j] != texto[i] || palabraClave[j+1] == '\0' && texto[i+1]>='a' && texto[i+1]<='z' ){
                    palabraClaveUsada = FALSE;
                }
                i++;
                j++;
            }
        }else{
            i++;
        }
    }
    

    if(palabraClaveUsada){
        printf("\nLa palabra clave aparece");
    }else{
        printf("\nNo aparece la palabra clave");
    }

    printf("\n\n");
    return 0;
}



