#include <stdio.h>

/**
 * DesComentar este bloque en linux
 */
 
/*
#include <stdio_ext.h>
int fflush(FILE * bufferAborrar){ __fpurge(bufferAborrar); return 0; }
*/


const int MAX = 4; 
typedef char String[MAX];

int main(){

    String cosa;
    printf("Ingresate algo: \n");

    int i = 0;
    do{
        fflush(stdin);
        scanf("%c", &cosa[i]);
        if(cosa[i] == '\n')
            cosa[i] = '\0';
        i++;
    }while(i<MAX && (cosa[i-1] != '\n' && cosa[i-1] != '\0') );


    printf("\n");
    i = 0;
    while(i < MAX && cosa[i]!='\0'){
        printf("%i: ", i);
        printf("%c", cosa[i]);
        printf("\n");
        i++;
    }
    
    
    

    printf("\n");
    return 0;
}