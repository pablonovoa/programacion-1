#include <stdio.h>

int main(){

    int hIn, minIn, hOut, minOut, hLaburo, minLaburo;
    printf("Ingrese hora y minutos de entrada: ");
    scanf("%i%i", &hIn, &minIn);
    printf("\nIngrese hora y minutos de salida: ");
    scanf("%i%i", &hOut, &minOut);

    if(minIn < 0 || minIn > 59 ){
        printf("Los minutos de entrada estan mal ingresados");
    }else if(minOut < 0 || minOut > 59){
        printf("Los minutos de salida estan mal ingresados");
    }else if(hIn < 8 ){
        printf("La hora de entrada no puede ser menor a 8");
    }else if(hOut > 19) {
        printf("La hora de salida no puede ser mayor a 19");
    }else if( (hIn > hOut) || ((hIn == hOut) && (minIn > minOut)) ){
        printf("La hora de entrada no puede ser posterior a la de salda");
    }else{
        hLaburo = hOut - hIn;
        minLaburo = minOut - minIn;
        printf("Laburó %i hs y %i minutos", hLaburo, minLaburo);
    }

    printf("\n");
    return 0;
}