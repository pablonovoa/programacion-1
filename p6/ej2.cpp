#include <stdio.h>

/**
 * DesComentar este bloque en linux
 */
 
/*
#include <stdio_ext.h>
int fflush(FILE * bufferAborrar){ __fpurge(bufferAborrar); return 0; }
*/

const int N = 4;
typedef int arrInts[N];
typedef enum {FALSE, TRUE} boolean;



int main(){
    int i;

    //a
    printf("\n/------- Parte A ---------/\n");
    arrInts numA;
    for(i=0; i<N; i++){
        printf("Ingrese un numero: ");
        scanf("%i", &numA[i]);
    }
    //b
    printf("\n/------- Parte B ---------/\n");
    arrInts numB;
    int tempStore;
    for(i=0; i<N; i++){
        printf("Ingrese un numero: ");
        scanf("%i", &numB[i]);
        int j = i;
        while(j>0 && numB[j] < numB[j-1]){
            tempStore = numB[j-1];
            numB[j-1] = numB[j];
            numB[j] = tempStore;
            j--;
        }
    }
    //c
    printf("\n/------- Parte C ---------/\n");
    printf("Array Num parte a: ");
    printf("%i", numA[0]);
    for (i=1; i < N; i++){
        printf(", %i", numA[i]);
    }
    printf("\nArray Num parte b: ");
    printf("%i", numB[0]);
    for (i=1; i < N; i++){
        printf(", %i", numB[i]);
    }
    //d
    printf("\n/------- Parte D ---------/\n");
    int menor1, menor2;
    menor1 = numA[0];
    menor2 = numA[0];
    for(i=1; i<N; i++){
        if(numA[i] < menor1 && numA[i] < menor2)
            menor1 = numA[i];
        else if( (numA[i] < menor2 && numA[i] != menor1) || menor1 == menor2 )
            menor2 = numA[i];
    }
    printf("Los numeros mas chicos del array de la parte a son : \n%i y %i", menor1, menor2);
    //e
    printf("\n/------- Parte E ---------/\n");
    boolean arrAreTheSame = TRUE;
    i = 0;
    while (arrAreTheSame && i<N){
        if(numA[i] != numB[i])
            arrAreTheSame = FALSE;
        i++;
    }
    if(arrAreTheSame)
        printf("Los arrays A y B son iguales");
    else
        printf("Los arrays A y B son distintos");
    

    printf("\n");
    return 0;
}