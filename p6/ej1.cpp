#include <stdio.h>

const int N = 5;
typedef int ArrayInt[N];
typedef int ArrMayorMenor[2];
typedef enum {FALSE, TRUE} boolean;

int main(){

    //a
    ArrayInt inputInts;
    for (int i=0; i < N; i++){
        printf("\nIngrese un numeto entero: ");
        scanf("%i", &inputInts[i]);
    }
    
    //b
    printf("\nNúmeros ingresados: ");
    for (int i=0; i < N; i++){
        if(i == 0)
            printf("%i ", inputInts[i]);
        else
            printf("- %i ", inputInts[i]);
    }

    //c
    float promedio = 0;
    for (int i=0; i < N; i++){
        promedio = promedio + inputInts[i];
    }
    promedio = promedio/5;
    printf("\nPromedio: %.1f", promedio);
    
    //d
    int numD, arrPosition=0;
    boolean isDinArry = FALSE;

    printf("\nIngrese otro número: ");
    scanf("%i", &numD);

    do{
        if(inputInts[arrPosition] == numD)
            isDinArry = TRUE;  
        arrPosition++;
    } while (!isDinArry && arrPosition < N);

    if(isDinArry)
        printf("\nEl número %i SI está en el array", numD);
    else 
        printf("\nEl número %i NO está en el array", numD);
    

    //e
    ArrMayorMenor mayorInArr = {inputInts[0], 0};
    ArrMayorMenor menorInArr = {inputInts[0], 0};

    for (int i = 1; i < N; i++){
        if(inputInts[i] > mayorInArr[0]){
            mayorInArr[0] = inputInts[i];
            mayorInArr[1] = i;
        }

        if(inputInts[i] < menorInArr[0]){
            menorInArr[0] = inputInts[i];
            menorInArr[1] = i;
        }
    }

    printf("\nEl mayor es %i en la posición %i ", mayorInArr[0], mayorInArr[1]);
    printf("\nEl menor es %i en la posición %i ", menorInArr[0], menorInArr[1]);

    //f
    int numF, multipCount=0;
    printf("\nIngrese otro número: ");
    scanf("%i", &numF);
    for (int i = 0; i < N; i++){
        if(inputInts[i] % numF == 0){
            multipCount++;
        }
    }
    printf("\nEn el array hay %i multiplos de %i", multipCount, numF);

    //g
    int numG;
    printf("\nIngrese otro número entre 1 y %i: ", N-1);
    scanf("%i", &numG);
    if(numG > 0 && numG < N){
        for (int i = 0; i < N; i++){
        if(i % numG == 0)
            printf("\n%i en pos %i", inputInts[i], i);
        }
    }else{
        printf("\nNúmero ingresado fuera del rango");
    }

    //h
    int arrMidPos, temporalIntStore, currentLastPos;
    arrMidPos = N/2 - 1;
    
    for (int i = 0; i <= arrMidPos; i++){
        currentLastPos = N-1-i;
        temporalIntStore = inputInts[currentLastPos];
        inputInts[currentLastPos] = inputInts[i];
        inputInts[i] = temporalIntStore;
    }
    printf("\nArry invertido: ");
    for(int i = 0; i < N; i++){
        if(i==0)
            printf("%i", inputInts[i]);
        else 
            printf(", %i", inputInts[i]);
    }
    
    
    

    printf("\n\n");
    return 0;
}