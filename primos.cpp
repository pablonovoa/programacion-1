#include <stdio.h>

typedef enum {FALSE, TRUE} boolean;

boolean is_prime(int n){
    if (n <= 3){
        if(n>1)
            return TRUE;
        else  
            return FALSE;
    }else if (n%2 == 0 || n%3 == 0){
        return FALSE;
    }

    int i = 5;
    while (i * i <= n){
        if (n%i == 0 || n%(i + 2)== 0){
            return FALSE;
        }
        i = i + 6;
    }
    return TRUE;
}


int main(){

    unsigned int findPrimesCount, numCheck, divisor, stopPoint=50;
    boolean esPrimo;
    
    while(stopPoint > 0){ 
        numCheck = 2;
        findPrimesCount = 0;

        printf("\n\nCuantos primos queres imprimir? (ingrese 0 para terminar):   ");
        scanf("%i" , &stopPoint);
        
        printf("\n------------------------------\n");
        
        if (stopPoint > 0){
            do{
                esPrimo = TRUE;
                /*
                divisor = 2;
                if(numCheck%2 == 0 && numCheck != 2)
                    esPrimo = FALSE;
                else
                    divisor = 3;
                

                while(esPrimo && (numCheck > divisor) ){
                    if(numCheck%divisor == 0)
                        esPrimo = FALSE;
                    divisor += 2;                   
                };
                */
                esPrimo =  is_prime(numCheck);

                if(esPrimo){
                    if(findPrimesCount != 0)
                        printf(", ");
                    printf("%i", numCheck);
                    findPrimesCount++;
                }

                numCheck++;

            }while(findPrimesCount < stopPoint);
        }else{
            printf("Adios");
        }
        
        printf("\n------------------------------\n");
    }
    return 0;
}