#include <stdio.h>

const float IVA = 0.22;
const float PI = 3.1416;

int main(){
    printf("\n---------|| Práctico 3 - Ej 2 ||---------\n");
    // parte a
    printf("\n---|| Parte A ||---\n");
    printf("Este es un mensaje.\n");

    // parte b
    printf("\n---|| Parte B ||---\n");
    printf("Esta es la primer linea.\nEsta es la segunda linea.\n");
    
    // parte c
    printf("\n---|| Parte C ||---\n");
    int num;
    printf("Ingrese un número entero: ");
    scanf("%d", &num);
    printf("El número ingresado es: %d \n", num);

    // parte d
    printf("\n---|| Parte D ||---\n");
    float precio, ivaMnt, total;
    printf("Ingrese un precio: ");
    scanf("%f", &precio);
    ivaMnt = precio * IVA;
    total = ivaMnt + precio;
    printf("Sub-Total: %.2f", precio);
    printf("\nIVA: %.2f", ivaMnt);
    printf("\nTotal: %.2f \n", total);

    // parte E
    printf("\n---|| Parte E ||---\n");
    int radio;
    float area;
    printf("Ingrese un valor de radio: ");
    scanf("%d", &radio);
    area = radio * radio * PI;
    printf("El área del curculo de radio %d es: %.4f", radio, area);

    printf("\n");
    return 0;
}



