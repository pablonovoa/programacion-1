#include <stdio.h>

const int DIAS = 7;
const float PI = 3.1416;
const float DISTANCIA = 189.9;
const int MESES = 12;

int main(){
    printf("\n------|| P1-E1 ||-------\n");
    printf("DIAS: %d", DIAS);
    printf("\nPI: %5.4f", PI);
    printf("\nDISTANCIA: %.1f", DISTANCIA);
    printf("\nMESES: %d", MESES);
    printf("\n------|| END P1-E1 ||-------\n");

    return 0;
}



